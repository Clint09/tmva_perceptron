#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TRandom.h>
#include <TH2.h>
#include <TString.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int dataTreeGenerator( TTree* tree, int n_events, double sigma, double mean_x, double mean_y ) {
    float square[2] = { 10, 10 }; // limits of data values: {x,y}
    double x, y;
    int event; 
    tree->Branch("x", &x, "x/D");
    tree->Branch("y", &y, "y/D");
    for( event = 0; event < n_events; event++ ) {
            do {
                x = gRandom->Gaus(mean_x, sigma);
                y = gRandom->Gaus(mean_y, sigma);
            } while ( x < 0 || x > square[0] || y < 0 || y > square[1] );
            tree->Fill();
    }
    
    return 0;
}

int dataBackgroundGenerator( TTree* tree, int n_events ) {
    int square[2] = { 10, 10 }; // limits of data values: {x,y}
    double x, y, w;
    int event;
    tree->Branch("x", &x, "x/D");
    tree->Branch("y", &y, "y/D");
    tree->Branch("weight", &w, "weight/D");
    for( event = 0; event < n_events; event++ ) {
        x = gRandom->Rndm() * square[0];
        y = gRandom->Rndm() * square[1];
        w = gRandom->Uniform(0.8);
        tree->Fill();
    } 
    return 0;
}


int main() {
    const unsigned int n = 3; // number of point types
    int n_events = 300;
    double sigma[n] =  { 0.8, 1, 1.3 };
    double mean_x[n] = { 2., 3., 8. };
    double mean_y[n] = { 2., 8., 5. };
    TString fileName = "data.root";
    TFile *dataFile = new TFile( fileName, "RECREATE" );
    for( unsigned int i = 0; i < n; i++ ) {
        char treeName[64], description[128];
        snprintf( treeName, sizeof(treeName), "tree%d", i );
        snprintf( description, sizeof(description), "learning data points of type %d", i );
        TTree *tree = new TTree( treeName, description );
        dataTreeGenerator( tree, n_events, sigma[i], mean_x[i], mean_y[i] );
        tree->Write();
    }
/*    TTree *tree = new TTree( "backgroundTree", "background data" );
    dataBackgroundGenerator( tree, n_events );
    tree->Write(); 
    dataFile->Close();
*/  

    double data_sigma[n] = { 2, 3, 4 };
    double x, y;
    float x1, y1;
    int event;
    unsigned int type;
    fileName = "app_data.root";
    TString treeName = "dataTree";
    dataFile = new TFile( fileName, "RECREATE" );
    TTree *tree = new TTree( treeName, "data for classification");  // redefining
    tree->Branch("x", &x, "x/D");
    tree->Branch("y", &y, "y/D");
    for( unsigned int i = 0; i < n; i++ ) {
        for( event = 0; event < n_events; event++ ) {
            x = gRandom->Gaus( mean_x[i], data_sigma[i] );
            y = gRandom->Gaus( mean_y[i], data_sigma[i] );
            tree->Fill();
        }
    }
    tree->Write();
    dataFile->Close();
    
    Double_t square[2] = { 10, 10 };
    fileName = "app_float_data.root";
    treeName = "dataTree";
    dataFile = new TFile( fileName, "RECREATE" );
    tree = new TTree( treeName, "data for classification");  // redefining
    TH2D *dataHist = new TH2D("dataHist", "points of all 3 types", 100, 0, 10, 100, 0, 10);
    tree->Branch("x", &x1, "x/F");
    tree->Branch("y", &y1, "y/F");
    tree->Branch("type", &type, "type/i");
    tree->Branch("hist", "TH2D", &dataHist, 128000, 0);
    for( unsigned int i = 0; i < n; i++ ) {
        type = i+1;
        for( event = 0; event < n_events; event++ ) {
            do {
                x1 = gRandom->Gaus( mean_x[i], data_sigma[i] );
                y1 = gRandom->Gaus( mean_y[i], data_sigma[i] );
            } while ( x1 < 0 || x1 > square[0] || y1 < 0 || y1 > square[1] );
            tree->Fill();
            dataHist->Fill( x1, y1 );
        }
    }
    tree->Write();
    dataHist->Write();
    dataFile->Close();
    return 0;

    return 0;
}
