
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TH1F.h"

#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Reader.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAMultiClassGui.h"

using namespace TMVA;
using namespace std;

TString getMLPStructure ( unsigned int layers, unsigned int *neurons ) {
    TString structure = "HiddenLayers=";
    for( unsigned int i=0; i < layers; i++ ) {
        if( i != 0 ) structure += ",";
        char layerNumber[64];
        snprintf( layerNumber, sizeof(layerNumber), "%d", *(neurons+i) );
        structure += layerNumber;
    }
    return structure;
}

int classificate( unsigned int layers, unsigned int *neurons )  {

    TMVA::Tools::Instance();
    
    TString inputFileName  = "./data.root";
    TString outputFileName = "test.root";

    TFile *inputFile  = TFile::Open( inputFileName );
    TFile *outputFile = TFile::Open( outputFileName, "RECREATE" );

    TTree *inputTree1 = (TTree*)inputFile->Get( "tree0" );
    TTree *inputTree2 = (TTree*)inputFile->Get( "tree1" );
    TTree *inputTree3 = (TTree*)inputFile->Get( "tree2" );
//    TTree *backgroundTree = (TTree*)inputFile->Get( "backgroundTree" );
    
    TMVA::Factory *factory = new TMVA::Factory( 
        "TMVAClassification",
        outputFile,
        "!V:!Silent:Color:DrawProgressBar:AnalysisType=Multiclass" );
    TMVA::DataLoader *dataLoader = new TMVA::DataLoader( "dataset" );
    
    dataLoader->AddVariable( "x", 'F' );
    dataLoader->AddVariable( "y", 'F' );
//    dataLoader->AddVariable( "sum := x+y", 'F' );
//    dataLoader->AddVariable( "dif := x-y", 'F' );
    dataLoader->AddTree( inputTree1, "type1", 1 );
    dataLoader->AddTree( inputTree2, "type2", 1 );
    dataLoader->AddTree( inputTree3, "type3", 1 );
//    dataLoader->AddSignalTree( inputTree1, 1 );
//    dataLoader->AddBackgroundTree( backgroundTree, 1 );
//    dataLoader->AddSignalTree( inputTree3, 1 );

//    dataLoader->SetBackgroundWeightExpression( "weight" );
    dataLoader->PrepareTrainingAndTestTree( "", "!V:NormMode=None" ); 
    TString bookOptions = "V:NCycles=200:" + getMLPStructure( layers, neurons );
    factory->BookMethod(dataLoader, TMVA::Types::kMLP, "MLP", bookOptions );
    factory->TrainAllMethods();
    factory->TestAllMethods();
    factory->EvaluateAllMethods();

    inputFile->Close();
    outputFile->Close();    
    std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;

    delete factory;
    delete dataLoader;
    
    return 0;
}

float application( ) {
        TString method = "MLP";

    //histograms options
    UInt_t nbin = 100; 
    Float_t probability, probabilityLimit = 0.5;
    
    int missType = 0, correctType = 0; 

    TMVA::Tools::Instance();
    
    TMVA::Reader *reader = new TMVA::Reader( "V:Color:!Silent:Error" );
    
    Float_t x, y, sum, dif;
    unsigned int type;
    Double_t efficiency = 0;
    reader->AddVariable( "x", &x );
    reader->AddVariable( "y", &y );
//    reader->AddVariable( "sum := x+y", &sum );
//    reader->AddVariable( "dif := x-y", &dif );

    TString dir = "dataset/weights/";
    TString prefix = "TMVAClassification";
    TString methodName = method; 
    TString weightFile = dir + prefix + "_" + methodName + ".weights.xml";
    reader->BookMVA( methodName, weightFile );

    TString histName = "MVA_" + method;
    TH1F *hist = new TH1F( histName, histName, nbin, 0., 1. );

    TString inputFileName = "./app_float_data.root";
    TFile *inputFile = TFile::Open( inputFileName );

    if (!inputFile) {
        std::cout << "ERROR: could not open data file" << endl;
        exit(1);
    }
    cout << "--- Select sample" << endl;
    TTree* tree = (TTree*)inputFile->Get( "dataTree" );
    tree->SetBranchAddress( "x", &x );
    tree->SetBranchAddress( "y", &y );
    tree->SetBranchAddress( "type", &type );
    
    Long64_t n_entries = tree->GetEntries();
    for( Long64_t i=0; i < n_entries; i++ ) {
        tree->GetEntry(i);
        probability = ( reader->EvaluateMulticlass( methodName ) )[0];
        hist->Fill( probability );
        if ( probability > probabilityLimit ) {
            if ( type == 1 ) correctType++;
            else missType++;
        }
        else {
            if ( type != 1 ) correctType++;
            else missType++;
        }

    }
    efficiency = (float) correctType / ( correctType + missType );
    std::cout << "number of correctly determined events: " << correctType << std::endl
              << "number of wrong determined events:" << missType << std::endl
              << "MLP efficiency: " << efficiency << std::endl;
    inputFile->Close();

    TFile *outputFile = new TFile( "app.root","RECREATE" );
    hist->Write();
    outputFile->Close();

    delete reader;

    return efficiency;
}

int main ( int argc, char** argv ) {
    ofstream file;
    file.open( "efficiency.txt" );
    
    for( int i = 0; i < 5; i++ ) {
        unsigned int layers = 1;
        unsigned int neurons[ layers ] = { 2 + i };
        classificate( layers, neurons );
        if( i == 0 ) file << application(); 
        else file << " & " << application();
    }
    file << " \\\\" << endl; 
    
    for( int j = 0; j < 5; j++ ) {
        for( int i = 0; i < 5; i++ ) {
            unsigned int layers = 2;
            unsigned int neurons[ layers ] = { 2 + j, 2 + i };
            classificate( layers, neurons );
            if( i == 0 ) file << application(); 
            else file << " & " << application();
        }
    file << " \\\\" << endl; 
    }

    file.close();
    return 0;
}
