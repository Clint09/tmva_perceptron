
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"

#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
//#include "TMVA/TMVAGui.h"
#include "TMVA/TMVAMultiClassGui.h"

int main( int argc, char** argv )  {
    
    bool gui = true;

    TMVA::Tools::Instance();
    
    TString inputFileName  = "./data.root";
    TString outputFileName = "perceptron.root";

    TFile *inputFile  = TFile::Open( inputFileName );
    TFile *outputFile = TFile::Open( outputFileName, "RECREATE" );

    TTree *inputTree1 = (TTree*)inputFile->Get( "tree0" );
    TTree *inputTree2 = (TTree*)inputFile->Get( "tree1" );
    TTree *inputTree3 = (TTree*)inputFile->Get( "tree2" );
//    TTree *backgroundTree = (TTree*)inputFile->Get( "backgroundTree" );
    
    TMVA::Factory *factory = new TMVA::Factory( 
        "TMVAClassification", 
        outputFile,
        "!V:!Silent:Color:DrawProgressBar:AnalysisType=Multiclass" );
    TMVA::DataLoader *dataLoader = new TMVA::DataLoader( "dataset" );
    
    dataLoader->AddVariable( "x", 'F' );
    dataLoader->AddVariable( "y", 'F' );
    dataLoader->AddTree( inputTree1, "type1", 1 );
    dataLoader->AddTree( inputTree2, "type2", 1 );
    dataLoader->AddTree( inputTree3, "type3", 1 );
//    dataLoader->AddSignalTree( inputTree1, 1 );
//    dataLoader->AddBackgroundTree( backgroundTree, 1 );
//    dataLoader->AddSignalTree( inputTree3, 1 );

//    dataLoader->SetBackgroundWeightExpression( "weight" );
    dataLoader->PrepareTrainingAndTestTree( "", "!V:NormMode=None" ); 
    factory->BookMethod(dataLoader, TMVA::Types::kMLP, "MLP", "!V:CalculateErrors=True:V:NCycles=200:HiddenLayers=N+5" );
    factory->TrainAllMethods();
    factory->TestAllMethods();
    factory->EvaluateAllMethods();

    inputFile->Close();
    outputFile->Close();    
    std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;

    delete factory;
    delete dataLoader;

//    char const* ch = "prceptron.root";

    if( gui ) TMVA::TMVAMultiClassGui( outputFileName );

}
