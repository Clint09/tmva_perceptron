#include <iostream>
#include <string>


#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TH1F.h"
#include "TROOT.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/Config.h"

using namespace TMVA;
using namespace std;

int main( int argc, char** argv ) {
    
    TString method = "MLP";

    //histograms options
    UInt_t nbin = 100; 
    Float_t probability, probabilityLimit = 0.5;
    
    int missType = 0, correctType = 0; 

    TMVA::Tools::Instance();
    
    TMVA::Reader *reader = new TMVA::Reader( "V:Color:!Silent:Error" );
    
    Float_t x, y;
    unsigned int type;
    Double_t efficiency = 0;
    reader->AddVariable( "x", &x );
    reader->AddVariable( "y", &y );

    TString dir = "dataset/weights/";
    TString prefix = "TMVAClassification";
    TString methodName = method; 
    TString weightFile = dir + prefix + "_" + methodName + ".weights.xml";
    reader->BookMVA( methodName, weightFile );

    TString histName = "MVA_" + method;
    TH1F *hist = new TH1F( histName, histName, nbin, 0., 1. );

    TString inputFileName = "./app_float_data.root";
    TFile *inputFile = TFile::Open( inputFileName );

    if (!inputFile) {
        std::cout << "ERROR: could not open data file" << endl;
        exit(1);
    }
    cout << "--- Select sample" << endl;
    TTree* tree = (TTree*)inputFile->Get( "dataTree" );
    tree->SetBranchAddress( "x", &x );
    tree->SetBranchAddress( "y", &y );
    tree->SetBranchAddress( "type", &type );
    
    Long64_t n_entries = tree->GetEntries();
    cout << "--- Processing: " << n_entries << " events" << endl;
    for( Long64_t i=0; i < n_entries; i++ ) {
        tree->GetEntry(i);
        probability = ( reader->EvaluateMulticlass( methodName ) )[0];
        hist->Fill( probability );
//        std::cout << type << " " << std::endl;
        if ( probability > probabilityLimit ) {
            if ( type == 1 ) correctType++;
            else missType++;
        }
        else {
            if ( type != 1 ) correctType++;
            else missType++;
        }

//        std::cout << probability << " ";
    }
    efficiency = (float) correctType / ( correctType + missType );
    std::cout << "number of correctly determined events: " << correctType << std::endl
              << "number of wrong determined events:" << missType << std::endl
              << "MLP efficiency: " << efficiency << std::endl;
    cout << "--- End of event loop: ";
    inputFile->Close();

    TFile *outputFile = new TFile( "app.root","RECREATE" );
    hist->Write();
    outputFile->Close();

    delete reader;
    return 0;
}
