#include <TMath.h>
#include <TRandom.h>
#include <iostream>
#include <fstream>
using namespace std;

int main() {
    int n_events = 900;
    int n_types = 3;
    int square[2] = { 10, 10 };
    double sigma[n_types] =  { 1., 2., 3. };
    double mean_x[n_types] = { 2., 3., 8. };
    double mean_y[n_types] = { 2., 8., 5. };
    double x, y;
    int event, t; 
    ofstream file;
    file.open( "data.txt");
    for( event = 0; event < n_events / n_types; event++ ) {
        for( t = 0; t < n_types; t++ ) {
            do {
                x = gRandom->Gaus(mean_x[t], sigma[t]);
                y = gRandom->Gaus(mean_y[t], sigma[t]);
            } while ( x < 0 || x > 10 || y < 0 || y > 10 );
            file << x << " " << y << " " << t << "\n";
        }
    }
    file.close();
    return 0;
}
